# encoding: utf-8
# language: pt


Funcionalidade: Tentativa de Login com dados inválidos

    #CT-02
    @TesteLoginInvalido
    Cenário: Validar usuário e senha inválidos

        Quando digito usuário e senha INVÁLIDO e realizo login
        Então o sistema exibe um alerta Usuário e senha inválidos.