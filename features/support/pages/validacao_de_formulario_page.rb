# frozen_string_literal: true

require 'selenium-webdriver'

require 'open-uri'

class ValidacaoRelatorio < DefaultPage


    def select_button_report(rota)
        sleep 2
        find(:xpath, "//div[@class='info-box home white-bg home hover ng-scope'][contains(.,'#{rota}')]//i[@class='fa fa-file-pdf-o btn btn-table-action ']").click
    end

    def click_delivery_report
        click_button("Relatório de Entrega")
    end

    # def driver_options
    #     download_directory = './downloads/'
      
    #     { args: ['test-type', 'disable-extensions'],
    #       prefs: {
    #         plugins: { always_open_pdf_externally: true },
    #         savefile: { default_directory: download_directory },
    #         download: { prompt_for_download: false,
    #                     default_directory: download_directory }
    #       }
    #     }
    # end

    # def pdf_download
    #     # element = find(:xpath,'//body')
    #     File.open('my_file_pdf','wb') do |file|
    #         file.write open('blob:http://py.martin-brower.com.br/*').read
    #     end
    #     # find(:xpath,'//*//embed')
    #     # .native.send_key(:control,'p')
    # end

    
end