# encoding: utf-8
# language: pt

@login
Funcionalidade: Buscar rotas disponíveis , em andamento e checklist

    #CT-05
    @ValidarCampoCD
    Cenário: Validar o campo CD
       
        Dado clico no campo CD exibe a lista de CD's
        Quando seleciono um CD "RP"
        Então o sistema exibe as rotas disponíveis para o CD selecionado

    #CT-07
    @ValidarRotasFinalizadas
    Cenário: Validar o botão Rota Finalizada

        Dado selecione o botão +Rotas Finalizadas sendo exibido a página ROTAS FINALIZADAS
        E escolha um CD, Data DE e ATÉ e o Tipo
        Quando atualizo
        Então o sistema exibe as rotas finalizadas para o cd "JC"

    #CT-08
    @ValidarBotaoRotasDisponiveis
    Cenário: Validar o botão Rota Disponíveis

        Dado selecione o icone + na coluna DISPONÍVEIS
        Então exibe o formulário para cadastrar uma nova rota

    #CT-09
    @ValidarCampoCodigo
    Esquema do Cenário: Validar o campo Código da Rota

        Dado selecione o icone + na coluna DISPONÍVEIS
        Quando digito um Código da rota "<code>"
        Então o campo aceita o preenchimento do código "<code>"
        Exemplos:
        | code   | 
        | BB201M |

    #CT-10
    @ValidarCampoPlaca
    Esquema do Cenário: Validar o Campo Placa

        E selecione o icone + na coluna DISPONÍVEIS
        E digito no campo "<plate>"
        Quando digito a "<truck_plate>"
        Então o campo aceita o preenchimento da placa "<plate>", "<truck_plate>"
        Exemplos:
        | plate   | truck_plate |
        | CVP8193 | CVP9274     | 

    #CT-11
    @ValidarCampoMotorista
    Esquema do Cenário: Validar o Campo Motorista  

        E selecione o icone + na coluna DISPONÍVEIS
        Quando digito no campo de nome do Motorista "<name>"
        Então o campo aceita o preenchimento do nome "<name>"
        Exemplos:
        | name                    |
        | AIRTON CARLOS FERNANDES |

    #CT-12-
    @ValidarCampoData
    Esquema do Cenário: Validador o Campo DaTA

        E selecione o icone + na coluna DISPONÍVEIS
        Quando digito uma "<data_faturamento>", "<data_entrega>" e "<data_retorno>"
        Então os campo de DATAS aceitam o preenchimento com a máscara dd mm aaaa "<data_faturamento>", "<data_entrega>", "<data_retorno>"
        Exemplos:
        | data_faturamento     | data_entrega     | data_retorno     |
        | today                | today            | today            |
        | today                |                  |                  |
        |                      | today            |                  |  
        |                      |                  | today            |

    #CT-016
    @ValidarMascaraHora
    Esquema do Cenário: Validar a hora esperada de chegada ao CD

        Dado selecione o icone + na coluna DISPONÍVEIS
        Quando digito uma hora retorno: "<hora_retorno>"
        Então o campo permite a digitação com a máscara hh:mm
        Exemplos:
        | hora_retorno |
        |     1212     |

    #CT-017
    @ValidarFormularioAdicionarEntrega
    Cenário: Validar o botão de Adicionar Entrega

        Dado selecione o icone + na coluna DISPONÍVEIS
        Quando clico em ADICIONAR na opção ENTREGAS
        Então exibe o formulário para Adicionar uma Entrega

    # CT-018
    @ValidarCodigoEntrega
    Esquema do Cenário: Validar o Codigo da entrega

        Dado selecione o icone + na coluna DISPONÍVEIS
        E clico em ADICIONAR na opção ENTREGAS
        Quando digito o código da entrega: "<codigo_entrega>"
        Então o campo é preenchido e automaticamente preenche os campos UF e Cidade com: "<uf>" e "<cidade>"
        Exemplos:
        | codigo_entrega | uf |  cidade   |
        |    AGD-CVA     | SP | Catanduva |

    #CT-19
    @ValidarADataDaEntrega
    Esquema do Cenario: Validar a Data da Entrega

        Dado selecione o icone + na coluna DISPONÍVEIS
        E clico em ADICIONAR na opção ENTREGAS
        Quando seleciono uma Data "<data_entrega>"
        Então o campo preenche a data selecionada "<data_entrega>"
        Exemplos:
        | data_entrega |
        | today        | 

    #CT-21
    @ValidarAHoraDaEntrega
    Esquema do Cenario: Validar a Hora da Entrega

        Dado selecione o icone + na coluna DISPONÍVEIS
        E clico em ADICIONAR na opção ENTREGAS
        Quando digito no campo "<hora_entrega>" uma hora desejada
        Então o campo preenche com a hora digitada no formato hh:mm
        Exemplos:
        | hora_entrega |
        | 1222         |

    #CT-20
    @ValidarTipoEntrega
    Cenário: Validar o Tipo de Entrega
        Dado selecione o icone + na coluna DISPONÍVEIS
        E clico em ADICIONAR na opção ENTREGAS
        Quando seleciono o Tipo
        Então o campo preenche com o tipo selecionado

    #CT-22
    @ValidarCampoSalvar
    Esquema do Cenário: Validar o campo salvar
        Dado que eu esteja no formulário rota + nova rota
        E clico em adicionar na opção ENTREGAS
        E na aba pedidos selecionar ADICIONAR
        E preencho o campo NF e salvo
        E o pedido é exibido na aba pedidos 
        E vou em salvar para registro da entrega 
        Quando salvo a rota: "<plate>", "<name>"
        |code|<code>|
        Então o sistema registra a rota 
        |code|<code>|
        Exemplos:
        | code         |plate       |name                       |
        |auto-generated|CVP8193     |AIRTON CARLOS FERNANDES    |
    