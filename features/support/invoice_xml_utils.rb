require 'ox'

class InvoiceXmlUtils

    XML_TEMPLATE_PATH = File.join(File.dirname(__FILE__), "../../files/invoice-template.xml")

    attr_reader :doc

    def initialize
        @doc = Ox.parse(File.open(XML_TEMPLATE_PATH).read)        
    end

    def product
        @doc.nfeProc.NFe.locate('*/det/prod')[0]
    end

    def remove_products!
        @doc.nfeProc.NFe.locate('*/det')[0].remove_children_by_path('prod')
    end 

    def add_product_to_invoice!(product)
        @doc.nfeProc.NFe.locate('*/det')[0] << product
    end
        
    def replace_product_values(product, new_product_hash)
        product.cProd.replace_text(new_product_hash["Código"])
        product.xProd.replace_text(new_product_hash["Descricao Produto"])
        product.qCom.replace_text("#{new_product_hash["Quantidade"]}.0000")
        product.vProd.replace_text(new_product_hash["Valor Total"])
        product.vUnCom.replace_text("#{new_product_hash["Valor Unitario"]}00")        
        product
    end

    def write_to_file(full_file_path)
        xml_file = File.new(full_file_path, "w")
        xml_file.puts(Ox.dump(@doc))
        xml_file.close
    end

    
end
