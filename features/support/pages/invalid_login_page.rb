# frozen_string_literal: true

class InvalidLoginPage < DefaultPage

    element :invalid_login, "input[placeholder='Nome de Usuário']"
    element :invalid_password, "input[placeholder='Senha']"
    element :login_button, :xpath, "//button[contains(.,'Login')][1]"
    
    def access_denied
        invalid_login.set "dasacf"
        invalid_password.set "23424342"
        login_button.click
    end

end
