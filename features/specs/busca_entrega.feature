# encoding: utf-8
# language: pt

@login
Funcionalidade: Buscar uma entrega 

     
    #CT-30
    @VerificarBuscaPeloCD
    Esquema do Cenário:  Verificar a busca pelo CD

        Dado que esteja na tela "Painel de Entrega"
        Quando Clicar na opção CD "JC"
        Então poderá selecionar o "<cd>" correspondente a consulta
        Exemplos:
        | cd |
        | JC |

    #CT-32
    @ValidarBotaoAtualizar
    Cenário: Validar botão atualizar

        Dado que esteja na tela "Painel de Entrega"
        Quando cicar no botão "Atualizar"
        Então aparecerá os dados gerais de entrega