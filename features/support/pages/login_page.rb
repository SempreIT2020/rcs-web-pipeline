# frozen_string_literal: true

class LoginPage < DefaultPage
  def load
    visit 'http://py.martin-brower.com.br/wbr/web-rcs/#/'
    
  end

  def with(user, pass)
    load
    find("input[placeholder='Login']").set user
    find(:xpath, "//input[@placeholder='Password']").set pass
    find(:xpath, "//button[contains(.,'Login')][1]").click
  end
end
