# frozen_string_literal: true

#CT-051
Dado("que esteja na tela transportadora") do
    @side.hamburger
    @side.link('Cadastro')
    @side.link('Transportadoras')
end

#CT-051
Quando("clicar em + NOVA TRANSPORTADORA") do
    @transporters.click_new_transporter
end

#CT-051
Então("abrira a tela para inserir os dados da transportadora") do
    expect(@transporters.validate_form_new_transporter).to be true
end

#CT-052
Então("o campo nome deve ser válido") do
    expect(@transporters.validate_name_field)
end


Quando("inserir uma transportadora") do |table|
    transporter = table.rows_hash
    @transporters.insert_random_name(transporter)
end


Quando("inserir o nome da transportadora {string}") do |transportadora|
    @transporters.insert_transporter_name(transportadora)
end

#CT-053
Então("a opção de ATIVO devera ser ticada") do
    @transporters.click_checkbox_active
    expect(@transporters.validate_checkbox_state).to be true
end

#CT-054
Então("deverá salvar a transportadora com sucesso") do
    @transporters.click_save_new_transporter
    expect(@transporters.validate_new_transporter)
end

Então("abrirá a tela para inserir um novo veiculo") do
    expect(@vehicles.validate_form_new_vehicle).to be true
end

Dado("que esteja na tela veículo") do
    @side.hamburger
    @side.link('Cadastro')
    @side.link('Veiculos')
end

E("clicar em + NOVO VEICULO") do
    @vehicles.click_new_vehicle
end

#CT-056
Quando("inserir a placa:") do |table|
    placa = table.rows_hash
    @vehicles.insert_vehicle_plate(placa)
end

#CT-056
E("selecionou a transportadora {string}") do |transportadora|
    @vehicles.set_transp(transportadora)
end

#CT-056
Então("salvara o veículo com sucesso") do
    @vehicles.save_new_vehicle
    expect(@vehicles.validate_new_vehicle).to be true
end