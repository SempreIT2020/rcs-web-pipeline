# frozen_string_literal: true

class DefaultPage < SitePrism::Page

  element :loading, :xpath, "//*[@id='loading-spinner-text']"
  
end
