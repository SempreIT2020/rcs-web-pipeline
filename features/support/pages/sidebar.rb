class Sidebar < DefaultPage

    def hamburger
        find(:xpath, '//button[@ng-click="ctrl.openNav()"]').click
      end
    
      # opção Portal
      def link(page_link)
        find(:xpath,"//nav//a[contains(.,'#{page_link}')]",).click
        
    end

  def validar_opcoes_cadastro
    page.has_xpath?('//nav//a[contains(text(),"Transportadoras")]')
    page.has_xpath?('//nav//a[contains(text(),"Veiculos")]')
    page.has_xpath?('//nav//a[contains(text(),"Motoristas")]')
    page.has_xpath?('//nav//a[contains(text(),"Controle de Rota")]')
  end

end