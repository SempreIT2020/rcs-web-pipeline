require 'pdf/inspector'

def pdf_file_strings(file_path)
    PDF::Inspector::Text.analyze_file(file_path).strings
end