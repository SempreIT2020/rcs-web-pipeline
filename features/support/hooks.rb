# frozen_string_literal: true

require 'report_builder'
require 'date'
# require 'win32/screenshot'
require 'dotenv'
require_relative 'env'

Dotenv.load
logs_folder_path = logs_folder

Before do
  @login = LoginPage.new
  @side = Sidebar.new
  @invalid_login = InvalidLoginPage.new
  @navigability = NavegabilidadePage.new
  @search_boarding = BuscaEmbarquePage.new
  @vehicles = VeiculosPage.new
  @search_delivery = BuscaEntregaPage.new
  @transporters = TransportadorasPage.new
  @reportvalidation = ValidacaoRelatorio.new
  @routecontrol = ControleDeRota.new
end

Before('@login') do
    @login.load
    expect(page).to have_title('Martin Brower - Portal de Aplicações')
    @login.with('tdsilva', 'sempreit0220')
    # logged_user = @top.logged_user?('ctorresi')
    # expect(logged_user).to be true
end

After do |scenario|
  #   expect(page).to have_no_xpath("//*[@id='loading-spinner-text']", wait:50)
  #   evidence_file_path = "#{logs_folder_path}/#{scenario.name}#{execution_time_str()}.png"
  #   Win32::Screenshot::Take.of(:desktop).write(evidence_file_path)
  #   shot = Base64.encode64(File.open(evidence_file_path, 'rb').read)
  #   embed(shot, 'image/png', 'Evidencia')
  # end

  # at_exit do
  #   report_path = "#{logs_folder_path}/report"
  #   ReportBuilder.configure do |config|
  #     config.input_path = "logs/report.json"
  #     config.report_path = report_path
  #     config.report_types = [:html]
  #     config.report_tabs = %w[Overview Features Scenarios Errors]
  #     config.report_title = 'Martin Brower'
  #     config.compress_images = true
  #     config.additional_info = { 'Sistema' => 'Test', 'Plataforma' => 'Web', 'Execucao' => DateTime.now.new_offset(Rational(-3, 24)).to_s }
  #     config.color = 'indigo'
  #   end
  #   ReportBuilder.build_report
  #   save_report_snapshot("#{report_path}.html")
end

After do 
  temp_shot = page.save_screenshot("logs/temp_shot.png")
  shot = Base64.encode64(File.open(temp_shot, 'rb').read)
  embed(shot, 'image/png', 'Evidencia')
end
