# frozen_string_literal: true

require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'site_prism/all_there'
require_relative 'pages/default_capybara'
require_relative 'utils'
require 'rspec'


execution_time = execution_time_str()
$logs_folder = "logs/#{execution_time}"
create_path($logs_folder)
$download_path = File.expand_path("#{$logs_folder}/downloads", Dir.pwd)
download_path_chrome = $download_path.gsub(File::SEPARATOR, File::ALT_SEPARATOR || File::SEPARATOR)

def logs_folder()
  $logs_folder
end

def download_folder()
  $download_path
end

# Capybara.register_driver :chrome do |app|
#   options = Selenium::WebDriver::Chrome::Options.new(
#     {args: ["headless", "disable-grpu","no-sandbox","disable-dev-shm-usage"]}
#   )
  
#   chromedriver_path = File.join(File.absolute_path('../..', File.dirname(__FILE__)),"chromedriver","chromedriver.exe")
#   Selenium::WebDriver::Chrome::Service.driver_path = chromedriver_path
#     Capybara::Selenium::Driver.new(app,
#     browser: :chrome,  
#     options: options,
#     # options: options,
#   ) 
# end

Capybara.register_driver :selenium_chrome_headless do |app|
  Capybara::Selenium::Driver.load_selenium
  browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
    opts.args << '--headless'
    opts.args << '--disable-gpu' if Gem.win_platform?
    opts.args << '--no-sandbox'
    opts.args << '--disable-site-isolation-trials'
    # chromedriver_path = File.join(File.absolute_path('../..', File.dirname(__FILE__)),"chromedriver","chromedriver.exe")
    #  Selenium::WebDriver::Chrome::Service.driver_path = chromedriver_path
  end
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
end

# Capybara.register_driver :chrome do |app|
#   options = Selenium::WebDriver::Chrome::Options.new
#   options.add_argument("--start-maximized", "--headless")
  
#   options.add_argument("--enable-features=NetworkService,NetworkServiceInProcess")    
#   options.add_preference('download.default_directory', download_path_chrome)
#   Capybara::Selenium::Driver.new(app,
#     browser: :chrome,
#     options: options 
#   )
# end


Capybara.configure do |c|
  c.default_driver = :selenium_chrome_headless
  c.default_max_wait_time = 50
  Capybara.ignore_hidden_elements = true
end
