

$download_folder = download_folder()
$timeout = 50

def pdf_downloads(file_name)
    Dir["#{$download_folder}/#{file_name}*.pdf"]    
end

def wait_for_download(file_name)
    Timeout.timeout($timeout) do
        sleep 0.1 until downloaded?(file_name)
    end
end

def downloaded?(file_name)
    pdf_downloads(file_name).any?
end


