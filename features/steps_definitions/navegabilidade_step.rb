# frozen_string_literal: true

# CT-03
Dado('que eu clique no menu') do
  @side.hamburger
end

# CT-03
Quando('seleciono a opção {string}') do |page_link|
  @side.link(page_link)
end

# CT-03
Então('o sistema desconecta a sessão do usuário e volta para a tela de LOGIN') do
  expect(page).to have_xpath("//hgroup/h2[contains(.,'Portal Martin Brower')]")
end

# CT-04
Dado('que eu esteja logado em página diferente da Embarque {string}') do |page_link|
  @side.hamburger
  @navigability.alternative_page(page_link)
end

# CT-04
Quando('seleciono a opção pelo menu lateral {string}') do |page_link|
  @side.hamburger
  @navigability.alternative_page(page_link)
end
# CT-04
Então('carrega a página Embarque com a lista de rotas disponíveis, em andamento e check list') do
  expect(page).to have_xpath("//button[contains(.,'Embarque')]")
end

# CT-23
Quando('clico na opção cancelar') do
  @navigability.button_cancel_form_delivery
end

# CT-23
Então('o sistema fecha o formulário retornando para a tela EMBARQUE') do
  expect(page).to have_xpath("//button[contains(.,'Embarque')]")
end

# CT-27
Quando('Clico na opção movimento da rota {string} da dats {string} na coluna em Andamento') do |date_route, route|
  @navigability.select_route_in_progress(date_route, route)
end

# CT-27
Então('abrira todas as informações da rota.') do
  expect(page).to have_xpath("//h3[contains(.,'Movimentação da Rota')]")
end

# CT-28
Dado('que na coluna Check List seleciono rota {string} da data {string} há opção {string}') do |route, date_route, button|
  @navigability.select_route_in_checklist(route, date_route, button)
end

# CT-28
Então('clicando na opção abrira a tela com os dados {string}, {string}, {string}') do |delivery_occurrences, tray_check, confirm_checklist|
  expect(@navigability.validate_button_deliveryocurrences(delivery_occurrences))
  expect(@navigability.validate_button_traycheck(tray_check))
  expect(@navigability.validate_button_confirmchecklist(confirm_checklist))
end

Então('expandirá os tópicos Transportadora, Veiculo, Motorista e Controle de rota') do 
  expect(@side.validar_opcoes_cadastro).to be true
end

#CT-29
Dado("clicar na opção T, abrira tela com as opções referente a rota especifica") do
  @navigability.click_transport_check
end

Então("poderá visualizar as informações como {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}") do |hodometro, entrega, ocorrencia_cliente, ocorrencia_na_rota, abastecimento, campanha, paradas, cart_entrega, cart_retorno, confirmar_checklist|
  expect(@navigability.validate_check_hodometro?(hodometro))
  expect(@navigability.validate_check_entregas?(entrega))
  expect(@navigability.validate_check_custumer_occurrence?(ocorrencia_cliente))
  expect(@navigability.validate_check_custumer_route?(ocorrencia_na_rota))
  expect(@navigability.validate_check_supply?(abastecimento))
  expect(@navigability.validate_check_campaign?(campanha))
  expect(@navigability.validate_check_pauses?(paradas))
  expect(@navigability.validate_check_delivery_cart?(cart_entrega))
  expect(@navigability.validate_check_return_cart?(cart_retorno))
  expect(@navigability.validate_check_checklist_confirm?(confirmar_checklist))
end