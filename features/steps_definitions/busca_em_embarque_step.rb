# frozen_string_literal: true

# CT-005
Dado("clico no campo CD exibe a lista de CD's") do
  @search_boarding.open_list_cd
end

# CT-005
Quando('seleciono um CD {string}') do |cd|
  @search_boarding.select_option_cd(cd)
end

# CT-005
Então('o sistema exibe as rotas disponíveis para o CD selecionado') do
  @search_boarding.wait_until_loading_invisible
  expect(page).to have_xpath('//option[@selected="selected"][contains(.,"RP")]')
end

# CT-007
Dado('selecione o botão +Rotas Finalizadas sendo exibido a página ROTAS FINALIZADAS') do
  @search_boarding.select_finished_route
  @search_boarding.wait_until_loading_invisible
end

# CT-007
E('escolha um CD, Data DE e ATÉ e o Tipo') do
  @search_boarding.select_cd_in_finished_route
  @search_boarding.select_search_filters
end

# CT-007
Quando('atualizo') do
  @search_boarding.refresh_button
  @search_boarding.wait_until_loading_invisible
end

# CT-007
Então('o sistema exibe as rotas finalizadas para o cd {string}') do |cd|
  @search_boarding.count_finished_route.each do |title|
    expect(title.text).to include cd
  end
  @search_boarding.wait_until_loading_invisible
end

# CT-08-09-10-11-12
Dado('selecione o icone + na coluna DISPONÍVEIS') do
  @search_boarding.select_button_add_more
  @search_boarding.wait_until_loading_invisible
end

# CT-08
Então('exibe o formulário para cadastrar uma nova rota') do
  expect(page).to have_xpath("//div/h4[contains(.,'Rota')]")
end

# CT-09
Quando('digito um Código da rota {string}') do |code|
  @search_boarding.select_code_field(code)
  @search_boarding.wait_until_loading_invisible
end

# CT-09
Então('o campo aceita o preenchimento do código {string}') do |code|
  expect(@search_boarding.has_secret_value?(code))
end

# CT-10
Dado('digito no campo {string}') do |plate|
  @search_boarding.input_plate_code(plate)
end

# CT-10
Quando('digito a {string}') do |truck_plate|
  @search_boarding.input_truck_plate_code(truck_plate)
end

# CT-10
Então('o campo aceita o preenchimento da placa {string}, {string}') do |plate, truck_plate|
  expect(@search_boarding.has_secret_value_plate?(plate))
  expect(@search_boarding.has_secret_value_truck_plate?(truck_plate))
end

# CT-11
Quando('digito no campo de nome do Motorista {string}') do |name|
  @search_boarding.input_driver_name(name)
end

# CT-11
Então('o campo aceita o preenchimento do nome {string}') do |name|
  expect(@search_boarding.has_secret_value_driver_name?(name))
end

# CT-12
Quando('digito uma {string}, {string} e {string}') do |data_faturamento, data_entrega, data_retorno|
  @search_boarding.input_dates(data_faturamento, data_entrega, data_retorno)
end

# CT-12
Então('os campo de DATAS aceitam o preenchimento com a máscara dd mm aaaa {string}, {string}, {string}') do |data_faturamento, data_entrega, data_retorno|
  expect(@search_boarding.validate_value_billing_date?(data_faturamento))
  expect(@search_boarding.validate_value_delivery_date?(data_entrega))
  expect(@search_boarding.validate_value_return_date?(data_retorno))
end

Então('os campo de DATAS aceitam o preenchimento com a máscara dd mm aaaa') do
  expect(@search_boarding.validate_inserting_dates).to be(true), 'Campos não estão validando data'
end

# CT-016
Quando ('digito uma hora retorno: {string}') do |hora_retorno|
  @search_boarding.input_hour(hora_retorno)
end

# CT-016
Então ('o campo permite a digitação com a máscara hh:mm') do
  expect(@search_boarding.validate_mask_hour).to be(true), 'Campo de Hora não está sendo validado'
end
# CT-017--18-19
Quando('clico em ADICIONAR na opção ENTREGAS') do
  @search_boarding.click_add_delivery
end

# CT-017
Então('exibe o formulário para Adicionar uma Entrega') do
  expect(@search_boarding.validate_form_add_delivery).to be(true), 'O Formulário não foi encontrado'
end

# CT-018
Quando('digito o código da entrega: {string}') do |codigo_entrega|
  @search_boarding.insert_delivery_code(codigo_entrega)
end

# CT-018
Então('o campo é preenchido e automaticamente preenche os campos UF e Cidade com: {string} e {string}') do |uf, cidade|
  expect(@search_boarding.validate_delivery_locale(uf, cidade)).to be true
end

# CT-019
Quando('seleciono uma Data {string}') do |data_entrega|
  @search_boarding.form_delivery_input_date(data_entrega)
end

# CT-019
Então('o campo preenche a data selecionada {string}') do |data_entrega|
  expect(@search_boarding.form_delivery_validate_date?(data_entrega))
end

# CT-020
Quando('seleciono o Tipo') do
  @search_boarding.select_delivery_type
end

# CT-020
Então('o campo preenche com o tipo selecionado') do
  expect(@search_boarding.validate_selected_delivery_type).to be true
end

# CT-021
Quando('digito no campo {string} uma hora desejada') do |hora_entrega|
  @search_boarding.form_delivery_input_hour(hora_entrega)
end

# CT-021
Então('o campo preenche com a hora digitada no formato hh:mm') do
  expect(@search_boarding.form_delivery_validate_hour).to be(true), "Campo de Hora não está sendo validado"
end

#CT-22
Dado("que eu esteja no formulário rota + nova rota") do
    @search_boarding.select_button_add_more
end

#CT-22
E("clico em adicionar na opção ENTREGAS") do
  @search_boarding.click_add_delivery
end

#CT-22
E("na aba pedidos selecionar ADICIONAR") do 
  @search_boarding.fill_form_requests
  @search_boarding.set_date_delivery_request
  @search_boarding.select_delivery_type
  @search_boarding.set_hour_delivery_request
  @search_boarding.click_add_more_requests
end

#CT-22
E("preencho o campo NF e salvo") do
  @search_boarding.fill_invoice
end

#CT-22
E("o pedido é exibido na aba pedidos") do
  @search_boarding.validate_request?
end

#CT-22
E("vou em salvar para registro da entrega") do
  @search_boarding.save_for_delivery_record
end

#CT-22
Quando("salvo a rota: {string}, {string}") do |plate,name,table|
  codigo = table.rows_hash
  @search_boarding.fill_dates_delivery_form
  @search_boarding.insert_code_name(codigo)
  @search_boarding.input_plate_code(plate)
  @search_boarding.input_driver_name(name)
  @search_boarding.save_route_delivery_request
  @search_boarding.justifications_mobile_and_piro
  @search_boarding.close_qr
end

#CT-22
Então("o sistema registra a rota") do |table|
  value = expect(page).to have_xpath("//div[@class='col-lg-4'][2]//..//div[@class='info-box home white-bg home hover  ng-scope']/div[@class='row'][contains(.,'#{@search_boarding.code_name[:code]}')]")
  puts(value)
end
