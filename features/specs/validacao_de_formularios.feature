# encoding: utf-8
# language: pt

@login
Funcionalidade: Abrir relatório de Entrega

    #CT-25
    @ValidarRelatorioCheckList
    Esquema do Cenário: Validar o relatório de entrega CheckList

        Dado que na coluna Check List escolho uma rota "<rota>" há o ícone de relatório, ao clicar nele abrira duas opções 
        Quando  Clicar em Relatório de Entrega
        Entao Abrira um arquivo em PDF com as informações da entrega especifica
        Exemplos:
        | rota   |
        | NT719M | 

