# frozen_string_literal: true
require_relative '../utils'

class BuscaEmbarquePage < DefaultPage

    attr_reader :code_name

    element :finished_routes, :xpath, "//button[contains(.,'Rotas Finalizadas')]"
    element :select_date_init_finished_route, :xpath, "//input[@type='text'][@ng-model='searchData.dtInit']"
    element :select_date_end_finished_route, :xpath, "//input[@type='text'][@ng-model='searchData.dtEnd']"
    element :select_type_route, :xpath, "//select[@ng-model='searchData.carrierType']"
    element :select_option_type_route, "option[value='MB']" 
    element :select_refrest_button, :xpath, "//button[@ng-click='refresh()']"
    element :select_cd_finished_route, :xpath, "//select[@ng-model='searchData.currentCD']"
    element :select_option_cd_finished_route,"option[label='JC']"
    element :select_add_more, :xpath, "//button[@ng-click='openEditPopup()']"
    element :txtCode, :xpath, "//input[@name='txtCode']"
    element :txtPlate, :xpath, "//input[@name='txtVehicle']"
    element :txtTruck_plate, :xpath, "//input[@name='txtPlateTrailer']"
    element :confirm_txtPlate, :xpath, "//div[@ng-click='selectResult(result)']"
    element :input_billing_date, :xpath, "//input[@name='txtBilling']"
    element :input_delivery_date, :xpath, "//input[@name='txtDelivery']"
    element :input_return_date, :xpath, "//input[@name='txtReturn']"
    element :txtDriver_name, "input[name='txtDriver1']"
    element :input_return_hour, :xpath, "//input[@name='txtReturnH']"
    element :btn_add_delivery, :xpath, "//button[contains(text(),'Adicionar')]"
    element :input_delivery_code, :xpath, "//form[@name='formDelivery']//input[@id='_value']"
    element :input_delivery_city, :xpath, "//label[contains(text(),'Cidade')]//..//input"
    element :input_delivery_uf, :xpath, "//label[contains(text(),'UF')]//..//input"
    element :combo_delivery_type, :xpath, "//label[contains(text(), 'Tipo')]//..//select"
    element :input_date_form_delivery, :xpath, "//form[@name='formDelivery']//input[@type='text'][@max-view='date']"
    element :input_hour_form_delivery, :xpath, "//form[@name='formDelivery']//label[@for='input-id-1'][contains(.,'Hora')]//..//input"
    element :select_add_more_requests, :xpath, "//h3[contains(.,'Pedidos')]//..//button[contains(.,'Adicionar')]"
    element :set_delivery_code, :xpath, "//label[contains(.,'Código da Entrega')]//..//input"
    element :select_option_delivery_code, :xpath, "//label[contains(.,'Código da Entrega')]//..//div[@id='_dropdown']//..//div[@class='angucomplete-row ng-scope'][2]"
    element :save_request_delivery, :xpath, "//div[contains(text(),'Pedidos')]//..//..//..//..//button[contains(text(),'Salvar')]"
    element :input_date_delivery_request, :xpath, "//h4[contains(.,'Entrega')]//..//label[contains(.,'Data')]//..//input"
    element :input_hour_delivery_request, :xpath, "//h4[contains(.,'Entrega')]//..//label[contains(.,'Hora')]//..//input"
    element :save_route_delivery, :xpath, "//button[contains(.,'Salvar')]"
    element :mobile_justification, :xpath, "//label[contains(.,'Justificativa mobile')]//..//select"
    element :select_option_mobile_justification, :xpath, "//option[contains(.,'Sem mobile')]"
    element :pyrometer_justification, :xpath, "//label[contains(.,'Justificativa pirômetro')]//..//select"
    element :select_option_pyrometer_justification, :xpath, "//option[contains(.,'Sem piro')]"
    element :save_justification, :xpath, "//h3[contains(.,'Justificativa')]//..//..//..//button[contains(.,'Salvar')]"
    element :validate_saved_route_boarding, :xpath, ""
    element :close_qr_code, :xpath, "//button[contains(.,'Fechar')]"
    
    def open_list_cd
        find(:xpath, '//select[@ng-model="userData.currentCD"]').click
    end

    def select_option_cd(cd)
       unless cd.empty?
            find("option[label='#{cd}']").click 
       end
    end

    def select_finished_route
        finished_routes.click
    end

    def select_search_filters
        select_date_init_finished_route.set "15/01/2020"
        select_date_end_finished_route.set "30/01/2020"
        select_type_route.click
        select_option_type_route.click
        
        
    end

    def refresh_button
        select_refrest_button.click
    end
    def select_cd_in_finished_route
        select_cd_finished_route.click
        select_option_cd_finished_route.click
    end

    def count_finished_route
        all(:xpath, "//div[@class='col-sm-1 ng-binding'][contains(.,'JC')]")
    end

    def select_button_add_more
        select_add_more.click
    end
    
    def select_code_field(code)
        txtCode.click
        txtCode.set code
    end

    def has_secret_value?(value)
        txtCode.value == value
    end

    def has_secret_value_plate?(value_plate)
        txtPlate.value == value_plate
    end
    
    def has_secret_value_truck_plate?(value_truck_plate)
        txtTruck_plate.value == value_truck_plate
    end

    def input_plate_code(plate)
        txtPlate.click
        txtPlate.set plate
        confirm_txtPlate.click
    end

    def input_truck_plate_code(truck_plate)
        txtTruck_plate.click
        txtTruck_plate.set truck_plate
    end

    def input_driver_name(name)
        find("input[name='txtDriver1']").set name
        find(:xpath,"//div[@ng-click='selectResult(result)']").click
    end

    def has_secret_value_driver_name?(value_driver_name)
        txtDriver_name.value == value_driver_name
    end

    def input_dates(data_faturamento, data_entrega, data_retorno)
        unless data_faturamento.empty?
            # input_billing_date.click
            input_billing_date.set Time.new.strftime("%d/%m/%Y")
        end 
        unless data_entrega.empty?
            # input_delivery_date.click
            input_delivery_date.set Time.new.strftime("%d/%m/%Y")
        end
        unless data_retorno.empty?
            # input_return_date.click
            input_return_date.set Time.new.strftime("%d/%m/%Y")
        end

    end

    def billing_date
        input_billing_date.set Time.new.strftime("%d/%m/%Y")
    end

    def delivery_date
        input_delivery_date.set Time.new.strftime("%d/%m/%Y")
    end

    def return_date 
        input_return_date.set Time.new.strftime("%d/%m/%Y")
    end

    def validate_value_billing_date?(value_billing_date)
        input_billing_date.value == value_billing_date
    end

    def validate_value_delivery_date?(value_delivery_date)
        input_delivery_date.value == value_delivery_date
    end

    def validate_value_return_date?(value_return_date)
        input_return_date.value == value_return_date
    end

    def input_hour(hora_retorno)
        unless hora_retorno.empty?
            input_return_hour.set Time.new.strftime("%H%M")
        end
    end

    def input_hour_return_request
        input_return_hour.set Time.new.strftime("%H%M")
    end

    def validate_mask_hour
        !!(input_return_hour[:value]).capitalize.match(/\d\d:\d\d/)
    end
    def click_add_delivery
        btn_add_delivery.click
    end

    def validate_form_add_delivery
        page.has_xpath?("//div[@class='panel modal-content order-review modal-top']//div[text()='Entrega']")
    end

    def insert_delivery_code(codigo_entrega)
        input_delivery_code.set(codigo_entrega)
        find(:xpath, "//div[text()='#{codigo_entrega}']").click
    end

    def validate_delivery_locale(uf, cidade)
        input_delivery_city[:value] == cidade
        input_delivery_uf[:value] == uf
    end

    def select_delivery_type 
        within combo_delivery_type do
            find("option[label='ENTREGA']").click
        end
    end
    
    def validate_selected_delivery_type
        page.has_xpath?("//label[contains(text(), 'Tipo')]//..//select//option[@label='ENTREGA'][@selected='selected']")
    end

    def form_delivery_input_date(date)
        unless date.empty?
            date = sanitize_date(date)
        end   
        input_date_form_delivery.set date
    end

    def form_delivery_validate_date?(date)
        input_date_form_delivery.value == date
    end

    def form_delivery_input_hour(hour)
        input_hour_form_delivery.set hour
    end

    def form_delivery_validate_hour
        !!(input_hour_form_delivery[:value]).capitalize.match(/\d\d:\d\d/)
    end

    def click_add_more_requests
        select_add_more_requests.click
    end

    def fill_form_requests
        set_delivery_code.set "PAC"
        select_option_delivery_code.click
    end

    def fill_invoice
        find(:xpath,"//label[contains(text(),'NF')]//..//input").set "958488008"
        sleep 1
        find(:xpath,"//label[contains(.,'NF')]//..//div[@id='_dropdown']//..//div[@class='angucomplete-row ng-scope'][1]").click
        find(:xpath,"//label[contains(text(),'NF')]//..//..//..//..//button[contains(text(),'Salvar')]").click
    end

    def validate_request?
       get_value_request = find(:xpath,"//div[contains(text(),'Pedidos')]//../..//table").text
       puts get_value_request
    end

    def save_for_delivery_record
        save_request_delivery.click
    end

    def set_date_delivery_request
        input_date_delivery_request.set Time.new.strftime("%d/%m/%Y")
    end

    def set_hour_delivery_request
        input_hour_delivery_request.set Time.new.strftime("%H%M")
    end

    def fill_dates_delivery_form
        billing_date
        delivery_date
        return_date
        input_hour_return_request
    end

    def save_route_delivery_request
        save_route_delivery.click    
    end

    def initialize()
        @code_name = Hash.new
    end

    def insert_code_name(code_name)
        code_name = sanitize_name_code(code_name)

        txtCode.set code_name[:code]
        @code_name = code_name
    end

    def justifications_mobile_and_piro
        mobile_justification.click
        select_option_mobile_justification.click
        pyrometer_justification.click
        select_option_pyrometer_justification.click
        save_justification.click
    end

    def close_qr
        close_qr_code.click
    end
end