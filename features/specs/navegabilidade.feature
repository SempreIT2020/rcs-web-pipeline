# encoding: utf-8
# language: pt

@login
Funcionalidade: Navegação no portal rcs

    #CT-03
    @ValidarBotaoSairMenuLateral
    Cenário: Validar a funcionalidade do botão Sair

        Dado que eu clique no menu
        Quando seleciono a opção "Quit"
        Então o sistema desconecta a sessão do usuário e volta para a tela de LOGIN

    #CT-04
    @ValidarTelaEmbarque
    Cenário: Validar aba Embarque

        Dado que eu esteja logado em página diferente da Embarque "Painel de Entrega"
        Quando seleciono a opção pelo menu lateral "Embarque"
        Então carrega a página Embarque com a lista de rotas disponíveis, em andamento e check list
    
    #CT-23
    @ValidarOCampoCancelar
    Cenário: Validar o Campo Cancelar

        Dado selecione o icone + na coluna DISPONÍVEIS
        Quando clico na opção cancelar
        Então o sistema fecha o formulário retornando para a tela EMBARQUE

    #CT-27
    @ValidarOMovimentoDaRotaDoAndamento
    Cenário: Validar o movimento da rota do Andamento

        Quando Clico na opção movimento da rota "SP774M" da dats "21/10" na coluna em Andamento
        Então abrira todas as informações da rota.

    #CT-28
    @EntãoAbriraTodasAsInformaçõesDaRota
    Cenário: Então abrira todas as informações da rota

        Dado que na coluna Check List seleciono rota "NT719M" da data "16/09" há opção "A" 
        Então clicando na opção abrira a tela com os dados "Ocorrências na Entrega", "Verificação bandeja", "Confirmar CheckList"

    #CT-050
    @ValidarBotaoCadastro
    Cenário:  Validar se o botão cadastro expandira os tópicos (Transportadoras, veiculo, motorista)

        Dado que eu clique no menu
        Quando seleciono a opção "Cadastro"
        Então expandirá os tópicos Transportadora, Veiculo, Motorista e Controle de rota

    #CT-29
    @ValidarVerificacaoDoTransportesDoCheckList
    Esquema do Cenário: Validar Verificação do Transportes do Check List 

        Dado clicar na opção T, abrira tela com as opções referente a rota especifica 
        Então  poderá visualizar as informações como "<hodometro>", "<entregas>", "<ocorrencia_cliente>", "<ocorrencia_rota>", "<abastecimento>", "<campanha>", "<paradas>", "<cart_entrega>", "<cart_retorno>", "<confirmar_checklist>"
        Exemplos:
            | hodometro | entregas | ocorrencia_cliente | ocorrencia_rota    | abastecimento  | campanha | paradas | cart_entrega | cart_retorno | confirmar_checklist |
            | Hodometro | Entregas | Ocorrência Cliente | Ocorrencia na Rota | Abastecimento  | Campanha | Paradas | Cart Entrega | Cart Retorno | Confirmar CheckList |
   