# frozen_string_literal: true

Dado("que na coluna Check List escolho uma rota {string} há o ícone de relatório, ao clicar nele abrira duas opções") do |rota|
    @reportvalidation.select_button_report(rota)
end

Quando('Clicar em Relatório de Entrega') do
  @reportvalidation.click_delivery_report
  sleep 3
  @reportvalidation.pdf_download
end

Entao('Abrira um arquivo em PDF com as informações da entrega especifica') do
   sleep 3
end
