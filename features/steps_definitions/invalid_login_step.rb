# frozen_string_literal: true

#CT-02
Dado('digito usuário e senha INVÁLIDO e realizo login') do
  @login.load
  @invalid_login.access_denied
end

#CT-02
Então('o sistema exibe um alerta Usuário e senha inválidos.') do 
  sleep 2
  page.driver.browser.switch_to.alert.accept
end
