# frozen_string_literal: true

class NavegabilidadePage < DefaultPage
    
    element :check_hodometro, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Hodometro')]"
    element :check_entregas, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Entregas')]"
    element :check_ocorrencias_cliente, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Ocorrências Cliente')]"
    element :check_ocorrencias_rota, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Ocorrências na Rota')]"
    element :check_abastecimento, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Abastecimento')]"
    element :check_campanha, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Campanha')]"
    element :check_paradas, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Paradas')]"
    element :check_cart_entrega, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Cart Entrega')]"
    element :check_cart_retorno, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Cart Retorno')]"
    element :check_confirmar_checklist, :xpath, "//ul[@class='nav nav-tabs']//li[contains(.,'Confirmar CheckList')]"
    
    def alternative_page(page_link)
        find(:xpath,"//nav//a[contains(.,'#{page_link}')]").click
    end

    def button_cancel_form_delivery
        click_button "Cancelar"
    end
    
    def select_route_in_progress(date_route, route)
        find(:xpath, "//div[@class='row']//div[@class='col-sm-2 ng-binding'][contains(.,'#{route}')]//..//div[@class='col-sm-3 bold ng-binding'][contains(.,'#{date_route}')]//..//i[@uib-tooltip='Movimento da Rota']").click
    end

    def select_route_in_checklist(date_route, route, button)
        find(:xpath, "//div[@class='row']//div[@class='col-sm-2 ng-binding'][contains(.,'#{route}')]//..//div[@class='col-sm-3 bold ng-binding'][contains(.,'#{date_route}')]//..//div[@class='col-sm-1 row-btn'][contains(.,'#{button}')]").click
    end

    def validate_button_deliveryocurrences(delivery_occurrences)
        page.has_xpath?("//ul//a[contains(.,'#{delivery_occurrences}')]")
    end
    def validate_button_traycheck(tray_check)
        page.has_xpath?("//ul//a[contains(.,'#{tray_check}')]")
    end

    def validate_button_confirmchecklist(confirm_checklist)
        page.has_xpath?("//ul//a[contains(.,'#{confirm_checklist}')]")
    end

    def click_transport_check
        find(:xpath,"//i[contains(.,'T')]", match: :first).click
    end

    def validate_check_hodometro?(hodometro)
       check_hodometro.value == hodometro
    end

    def validate_check_entregas?(entrega)
        check_entregas.value == entrega
    end

    def validate_check_custumer_occurrence?(ocorrencia_cliente)
        check_ocorrencias_cliente.value == ocorrencia_cliente
    end

    def validate_check_custumer_route?(ocorrencia_na_rota)
        check_ocorrencias_rota.value == ocorrencia_na_rota
    end

    def validate_check_supply?(abastecimento)
        check_abastecimento.value == abastecimento
    end

    def validate_check_campaign?(campanha)
        check_campanha.value == campanha
    end

    def validate_check_pauses?(paradas)
        check_paradas.value == paradas
    end

    def validate_check_delivery_cart?(cart_entrega)
        check_cart_entrega.value == cart_entrega
    end

    def validate_check_return_cart?(cart_retorno)
        check_cart_retorno.value == cart_retorno
    end

    def validate_check_checklist_confirm?(confirmar_checklist)
        check_confirmar_checklist.value == confirmar_checklist
    end

end