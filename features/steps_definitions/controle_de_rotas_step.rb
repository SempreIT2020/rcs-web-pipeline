# frozen_string_literal: true

# CT-079
Dado('que esteja no menu lateral no campo {string} eu acesso {string}') do |page_link, page_link2|
    @side.hamburger
    @side.link(page_link)
    @side.link(page_link2)
end

# CT-079
E('clique em {string}') do |new_register|
  @routecontrol.click_button_new_register(new_register)
end

# CT-079
E('seleciono no campo Tipo a opção {string}') do |justification|
  @routecontrol.select_option_register(justification)
end

# CT-079
E("seleciono o CD {string} e preencho os campos Informe categoria e Informe detalhes") do |cd, table|
    rota = table.rows_hash
    @routecontrol.select_option_cd(cd)
    @routecontrol.set_category_and_details(rota)
end

# CT-079
Quando('clico em Salvar') do
  pending # Write code here that turns the phrase above into concrete actions
end

# CT-079
Entao('o campo aceita os dados e salva o registro') do
  pending # Write code here that turns the phrase above into concrete actions
end
