require_relative '../utils'


class TransportadorasPage < DefaultPage

    attr_reader :transporter_name

    element :btn_add_new_transporter, :xpath, "//button[contains(text(),'Nova transportadora')]"
    element :input_name, :xpath, "//div[@class='new-carrier content-detail']//input[@type='text']"
    element :checkbox_ativo, :xpath, "//div[@class='new-carrier content-detail']//input[@type='checkbox']"
    element :btn_save_new, :xpath, "//button[text()='SALVAR']"

    def initialize()
        @transporter_name = Hash.new
    end

    def insert_random_name(transporter_name)
        transporter_name = sanitize_name(transporter_name)

        input_name.set transporter_name[:transporter]
        @transporter_name = transporter_name
    end

    def click_new_transporter
        btn_add_new_transporter.click
    end

    def validate_form_new_transporter
        page.has_xpath?("//div[@class='new-carrier content-detail']//h1[text()='Nova transportadora']")
    end

    def insert_transporter_name(transporter_name)
        input_name.set(transporter_name)
    end

    def click_checkbox_active
        checkbox_ativo.click
    end

    def validate_checkbox_state
        page.has_xpath?("//input[@type='checkbox'][contains(@class,'ng-empty')]")
    end

    def validate_name_field
        page.has_xpath?("//div[@class='new-carrier content-detail']//input[@type='text'][contains(@class,'ng-valid-required')]")
    end

    def click_save_new_transporter
        btn_save_new.click
    end

    def validate_new_transporter
        sleep 2
        alert_text = page.driver.browser.switch_to.alert.text
        success_message = "CARRIER_SAVE_SUCESS"
        if alert_text == success_message
            page.accept_alert
            return true
        else
            return false
        end
    end
end