require_relative '../utils'
class ControleDeRota < DefaultPage

    attr_reader :register_route

    element :select_new_type_register, :xpath, "//label[contains(.,'Tipo')]//..//select"
    element :select_cd_list, :xpath, "//label[contains(.,'CD')]//..//select"
    element :input_category, :xpath, "//label[contains(.,'Informe a categoria')]//..//input"
    element :input_detail, :xpath, "//label[contains(.,'Informe o detalhe')]//..//input"

    def initialize()
        @register_route = Hash.new
    end

    def click_button_new_register(new_register)
        click_button("#{new_register}")
    end

    def click_type_register
        select_new_type_register.click

    end

    def select_option_register(justification)
        select_new_type_register.click
        find(:xpath,"//label[contains(.,'Tipo')]//..//select//..//option[contains(.,'#{justification}')]").click
    end

    def select_option_cd(cd)
        select_cd_list.click
        find(:xpath,"//label[contains(.,'CD')]//..//select//..//option[@value='#{cd}']").click
    end

    def set_category_and_details(register_route)
        register_route = sanitize_name_code(register_route)


        input_category.set register_route[:code]
        input_detail.set register_route[:code]

        @register_route = register_route
    end
end