# encoding: utf-8
# language: pt

@login
Funcionalidade: Cadastrar Rota

    @ValidarCriacaoDoCd
    Esquema do Cenário: Validar a criação do CD

    Dado que esteja no menu lateral no campo "Cadastro" eu acesso "Controle de Rota"
    E clique em "Novo cadastro"
    E seleciono no campo Tipo a opção "Justificativa"
    E seleciono o CD "FT" e preencho os campos Informe categoria e Informe detalhes
    |code|<code>|
    Quando clico em Salvar
    Entao o campo aceita os dados e salva o registro
    Exemplos:
    |code           | 
    |auto-generated | 
