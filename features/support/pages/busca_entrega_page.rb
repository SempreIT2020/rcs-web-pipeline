# frozen_string_literal: true

class BuscaEntregaPage < DefaultPage

    element :click_form_cd_options, :xpath, "//select[@ng-model='searchData.currentCD']"
    element :blue_graphic, :xpath, "//div[@class='info-box blue-bg']"
    element :green_graphic, :xpath, "//div[@class='info-box green-bg']"
    element :red_graphic, :xpath, "//div[@class='info-box red-bg']"

    def select_option_cd(cd)
        # click_form_cd_options.click
        find(:xpath,"//select/option[contains(.,'#{cd}')]").click
        sleep 1
    end

    def validate_select_option?(cd)
        validate = find(:xpath, "//option[@label][@selected][contains(.,'#{cd}')]")
        validate.value == cd    
    end

    def update_without_date(refresh_button)
        i = 0 
        for i in 0..11
            if i < 11
                find(:xpath, "//input[@ng-model='searchData.dtDelivery']").native.send_key(:backspace)    
            end
        end    
        click_button("#{refresh_button}")
        sleep 2
    end
    
    def validate_informations_delivery_blue_graphic?
        if blue_graphic != 0
            get_value_blue = blue_graphic.text
            puts(get_value_blue)
        end
    end

    def validate_informations_delivery_green_graphic?
        if green_graphic != 0
            get_value_green = green_graphic.text
            puts(get_value_green)
        end
    end

    def validate_informations_delivery_red_graphic?
        if red_graphic != 0
            get_value_red = red_graphic.text
            puts(get_value_red)
        end
    end
end
