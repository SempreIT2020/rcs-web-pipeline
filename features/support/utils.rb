require 'date'

def execution_time_str
  t = Time.new
  return t.strftime("%d%m%Y%k%M%S")
end

def create_path(path)
  FileUtils.mkdir_p path unless File.exists?(path)
end

def save_report_snapshot(html_report_path)
  Capybara.current_session.driver.visit File.absolute_path(html_report_path)
  report_snapshot = html_report_path.gsub("html", "png")
  Capybara.current_session.driver.find_css("div#overview")
  Capybara.current_session.driver.save_screenshot(report_snapshot)
end

def today_str()
  Time.new.strftime("%d/%m/%Y")
end 

def time_str()
  Time.new.time.strftime("%k:%M")
end

def generate_code(number)
  charset = Array('A'..'Z') + Array('a'..'z') + Array(0..9)
  Array.new(number) { charset.sample }.join
end

def generate_plate(number)
  charset = Array('A'..'Z') + Array(0..9)
  Array.new(number) { charset.sample }.join
end

def generate_name(number)
  charset = Array('A'..'Z') + Array('a'..'z')
  Array.new(number) { charset.sample }.join
end

def select_by_text(tag, text)
  return "//#{tag}[contains(.,'#{text}')]"
end

def today_plus(days)
  t = Date.today + days
  t.strftime("%d/%m/%Y")
end 

def sanitize_date(date)
  if date.eql?('today')
   return today_plus(0)
  end
  if date.include?('today_plus_')
    splited_value = date.split('_')
    plus_number = splited_value.last
    return today_plus(plus_number.to_i)
  end
  raise "Parâmetro #{date} inválido. Utilize today ou today_plus_n, onde n = número"
end

def sanitize_name(name)
  name.map { |key, value| 
    if ((key.eql?('plate')) and value.eql?('auto-generated'))
      value = generate_plate(4)
    end
    if (key.eql?('transporter') and value.eql?('auto-generated'))
      value = generate_name(6)
    end

    [key.to_sym, value]
  }.to_h
end 

def sanitize_name_code(name)
  name.map { |key, value| 
    if ((key.eql?('code')) and value.eql?('auto-generated'))
      value = generate_code(4)
    end
    
    [key.to_sym, value]
  }.to_h
end 

def sanitize_register_route(name)
  name.map { |key, value| 
    if ((key.eql?('categoria')) and value.eql?('entrega'))
      value = generate_code(6)
    end

    [key.to_sym, value]
  }.to_h
end 

def delete_all_files_in_path(path)
  Dir.foreach(path) do |f|
    fn = File.join(path, f)
    File.delete(fn) if f != '.' && f != '..'
  end


end

def is_numeric?(s)
  begin
    Float(s)
  rescue
    false
  else
    true
  end
  
end