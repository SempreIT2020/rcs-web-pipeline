# frozen_string_literal: true
#ct-30-32
Dado("que esteja na tela {string}") do |page_link|
    @side.hamburger
    @side.link(page_link)
end

#CT-30  
Quando("Clicar na opção CD {string}") do |cd|
    @search_delivery.select_option_cd(cd)
end

#CT-30  
Então("poderá selecionar o {string} correspondente a consulta") do |cd|
    expect(@search_delivery.validate_select_option?(cd))
end

#CT-32
Quando("cicar no botão {string}") do |refresh_button|
    @search_delivery.update_without_date(refresh_button)
end

#CT-32
Então("aparecerá os dados gerais de entrega") do
    expect(@search_delivery.validate_informations_delivery_blue_graphic?)
    expect(@search_delivery.validate_informations_delivery_green_graphic?)
    expect(@search_delivery.validate_informations_delivery_red_graphic?)
end
  