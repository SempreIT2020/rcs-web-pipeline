require_relative '../utils'


class VeiculosPage < DefaultPage

    attr_reader :plate_name
    
    element :btn_new_vehicle, :xpath, "//button[contains(text(),'Novo veículo')]"
    element :input_plate, :xpath, "//input[@placeholder='Veículo']"
    element :combo_transportadora, :xpath, "//div[@class='form-group']//select"
    element :btn_save_vehicle, :xpath, "//button[text()='SALVAR']"

    def initialize()
        @plate_name = Hash.new
    end
    
    def insert_vehicle_plate(plate_name)
        plate_name = sanitize_name(plate_name)

        input_plate.set plate_name[:plate]
        @plate_name = plate_name
    end
    
    def click_new_vehicle
        btn_new_vehicle.click
    end
    
    def set_transp(transportadora)
        within combo_transportadora do
            find(:xpath,"option[@label='#{transportadora}']").click
        end
    end

    def save_new_vehicle
        btn_save_vehicle.click
    end

    def validate_form_new_vehicle
        page.has_xpath?("//div[@class='new-vehicle content-detail']//h1[text()='Novo veículo']")
    end

    def validate_new_vehicle
        sleep 2
        alert_text = page.driver.browser.switch_to.alert.text
        success_message = "VEHICLE_SAVE_SUCESS"
        if alert_text == success_message
            page.accept_alert
            return true
        else
            return false
        end
    end
end