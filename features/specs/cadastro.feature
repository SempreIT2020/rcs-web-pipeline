# encoding: utf-8
# language: pt

@login
Funcionalidade: Validar Pagina de Cadastros

    #CT-051
    @ValidarTelaNovaTransportadora
    Cenário: Validar criação de nova transportadora
    
        Dado que esteja na tela transportadora
        Quando clicar em + NOVA TRANSPORTADORA
        Então abrira a tela para inserir os dados da transportadora


    #CT-052
    @ValidarNomeNovaTransportadora
    Cenário: Validar o campo Nome Nova Transportadora

        Dado que esteja na tela transportadora
        E clicar em + NOVA TRANSPORTADORA
        Quando inserir o nome da transportadora "Sbrubs"
        Então o campo nome deve ser válido

    #CT-053
    @ValidarAtivoNovaTransportadora
    Cenário: Validar a opção de Ativo na transportadora

        Dado que esteja na tela transportadora
        E clicar em + NOVA TRANSPORTADORA
        Quando inserir o nome da transportadora "Sbrubs"
        Então a opção de ATIVO devera ser ticada

    #CT-054
    @ValidarSalvarNovaTransportadora
    Esquema do Cenário: Validar o botão Salvar nova transportadora

        Dado que esteja na tela transportadora
        E clicar em + NOVA TRANSPORTADORA
        Quando inserir uma transportadora
        | transporter | <transporter> |
        Então deverá salvar a transportadora com sucesso
        Exemplos:
        | transporter  |
        |auto-generated|

    
    #CT-055
    @ValidarFormVeiculo
    Cenário: Validar o formulário de criação do veiculo
        Dado que esteja na tela veículo
        Quando clicar em + NOVO VEICULO
        Então abrirá a tela para inserir um novo veiculo

    #CT-056
    @ValidarCampoPlacaCadastro
    Esquema do Cenário: Validar o campo Placa

        Dado que esteja na tela veículo
        E clicar em + NOVO VEICULO
        Quando inserir a placa:
        | plate | <plate> |
        E selecionou a transportadora "TESTE"
        Então salvara o veículo com sucesso
        Exemplos:
        | plate         | 
        |auto-generated |